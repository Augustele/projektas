<?php
session_start(); //va sitoje vitoje yra isteigiama $_SESSION cart. Kai puslapis uzsikrauna tikrina ar tokia sesija yra sukurta, jei jos nera tai ji sukuriama
if(!isset( $_SESSION["cart"] ) || !is_array($_SESSION["cart"])) {
    $_SESSION["cart"] = [];
}

if(isset(   $_GET['logout'])){
    session_unset();
    session_destroy();

    $http = new Http();
    $http->redirectTo('/');
}

function dd($data){
    echo "<pre>";
    die(var_dump($data));
    echo "</pre>";
}

function array_field_sum($array, $field){
    $sum = 0;
    foreach ($array as $item){
        if(isset($item[$field])){
            $sum += $item[$field];
        }
    }
    return $sum;
}

