<?php

class UserModel
{
    /**
     * Funkcija, kuri gražina visas rezervacijas iš DB.
     *
     * @return array - rezervacijų masyvas
     */
    public function listAll()
    {
        // Susikuriame duombazes objekta
        $database = new Database();

        // Paruosiame uzklausa.
        $sql = 'SELECT * FROM User';

        // Siunciame uzklausa ir graziname gauta reiksme
        return $database->query($sql);
    }




    /**
     * Funkcija sukuria naują user įrašą DB.
     *
     * @param $data - iš formos siunčiami laukai
     * @return string - naujo DB įrašo ID (kas lygu true) arba false priklausomai nuo to, ar sėkmingai įterpė
     */
    public function create($data)
    {
        $database = new Database();

        $sql = "INSERT INTO `User` (`FirstName`, `LastName`,  `Phone`, `Email`, `Password`) VALUES (?, ?, ?, ?, ?)";

        $data_to_insert = [
            $data['name'],
            $data['surname'],
            $data['phone'],
            $data['email'],
            md5($data['password'])
        ];

        $result = $database->executeSql($sql, $data_to_insert);

        return $result;
    }

}
//gauna duomenis is kotrolerio ir iraso i DB




