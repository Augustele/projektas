<?php

class LoginModel
{

    public function listAll()
    {
        // Susikuriame duombazes objekta
        $database = new Database();

        // Paruosiame uzklausa.
        $sql = 'SELECT * FROM User';

        // Siunciame uzklausa ir graziname gauta reiksme
        return $database->query($sql);
    }


    //tikrinti ar toks user yra DB
    /**
     * @param $email
     * @param $password
     */
    public function canLogin($email, $password)
    {

        // Susikuriame duombazes objekta
        $database = new Database();

        // Paruosiame uzklausa.
        $sql = 'SELECT * FROM User WHERE Email = ?';

        $data = [
            $email
        ];

        // Siunciame uzklausa ir graziname gauta reiksme
        $user = $database->queryOne($sql, $data);

        if($user){
            // Useris egzistuoja, tikrinam passworda. Toliau i sesija sudedame duomenis kuriu mums reikia
            if($user['Password'] == md5($password)){
                // Password tinka
                $_SESSION['logged'] = true;
                $_SESSION['name'] = $user['FirstName'];
                $_SESSION['user_id'] = $user['Id']; //sito ID veliau mums reikes sesijoje tureti, nes reikes irasant orderio duomenis i DB
                $_SESSION['is_admin'] = $user['Role'] == 1;

                return true;
            }else{
                // Password netinka
                die('fail');
            }
        }else{
            // Useris neegzistuoja
            die('Useris neegzistuoja');
        }
        return false;
    }

}