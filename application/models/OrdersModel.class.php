<?php

class OrdersModel {

    public function listAll()
    {
        // Susikuriame duombazes objekta
        $database = new Database();

        // Paruosiame uzklausa
        $sql = 'SELECT * FROM Meal';

        // Siunciame uzklausa ir graziname gauta reiksme
        return $database->query($sql);
    }

    public function listProductId($productID)
    {
    // issaugoti sesijoje pasirinkto produkto ID
   // pasiimti produkto id is sesijos


   $database = new Database();

   $sql = 'SELECT * FROM Meal WHERE id = ' . $productID ;

   return $database->queryOne($sql);

   }

//cia toliau vyks uzsakymo duomenu surinkimas ir irasymas i DB


    public function create($data)
    {

        if (isset($_SESSION['user_id'])) {

            $database = new Database();

            $vat = 21;

            $sql = "INSERT INTO `Order` (`User_Id`, `TaxRate`, `TaxAmount`, `CompleateTimestamp`) VALUES (?, ?, ?, ?)";

            $data_to_insert = [
                $_SESSION['user_id'], //jis paprasytas yra pateikti i sesija Login modelyje
                $vat,
                array_field_sum($data, "tax"), //data - tai i kitamaji public create yra paduodama visa cart, o "tax" yra is CartControler
                time()
            ];

            $order_id = $database->executeSql($sql, $data_to_insert);

            foreach($data as $cart_item){
                $sql = "INSERT INTO `Orderline` (`Order_Id`, `Meal_Id`, `QuantityOrdered`, `PriceEach`) VALUES (?, ?, ?, ?)";

                $data_to_insert = [
                    $order_id,
                    $cart_item['id'],
                    $cart_item['quantity'],
                    $cart_item['total']
                ];

                $order_line_id = $database->executeSql($sql, $data_to_insert);

            }
        }

        else {
        $http = new Http(); //cia taip reikia aprasyti nes modelyje nezino kas tas $http, todel reikia sukurti nauja objekta, klases Http kuri yra jau framworke
        $http->redirectTo('/login');

    }
}



    public function delete($id)

    {
        // trinam is lenteles Meal produktus. Kaip gauti produkto ID? - tiesiai padaryti uzklausa i DB ir is ten pasiimti

         $database = new Database();

         $sql = "DELETE FROM `Meal` WHERE `id` = " . $id ;

         $result = $database->query($sql);

         return $result;

    }



   public function createMeal($data)
    {
        $file_name = '';


        if(isset($_FILES["fileToUpload"])){
            // Upload file
            // fileToUpload pavadinimas toks kaip formos pavadinime, o name - taip reikia pavadinti.

            $file_name = $_FILES["fileToUpload"]["name"];

            // Temporary file (in memory)
            $file_tmp =$_FILES['fileToUpload']['tmp_name'];

            // New file (to put on server). getcwd() suzino koks yra kelias projekte iki index katalogo paveiksliuku folderio, o toliau pati parasau nes ta formule iki galo neparodo kur paveiksliukus saugau.

            $target_dir = getcwd() . "/application/www/images/meals/";
            $target_file = $target_dir . $file_name;

            // Move temporary file (from memory) to server location
            move_uploaded_file($file_tmp, $target_file);
        }

        // Create DB record
        $database = new Database();

        $sql = "INSERT INTO `Meal` (`Name`, `Description`,  `QuantityInStock`, `BuyPrice`, `SalePrice`, `Photo`) VALUES (?, ?, ?, ?, ?, ?)";

        $data_to_insert = [
            $data['name'],
            $data['description'],
            $data['quantityInStock'],
            $data['buyPrice'],
            $data['salePrice'],
            $file_name
        ];


        $result = $database->executeSql($sql, $data_to_insert);

        return $result;
    }


}

