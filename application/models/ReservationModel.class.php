<?php

class ReservationModel {

    /**
     * Funkcija, kuri gražina visas rezervacijas iš DB.
     *
     * @return array - rezervacijų masyvas
     */
    public function listAll()
    {
        // Susikuriame duombazes objekta
        $database = new Database();

        // Paruosiame uzklausa
        $sql = 'SELECT * FROM Reservation';


        // Siunciame uzklausa ir graziname gauta reiksme
        return $database->query($sql);
    }

    /**
     * Funkcija sukuria naują rezervacijos įrašą DB.
     *
     * @param $data - iš formos siunčiami laukai
     * @return string - naujo DB įrašo ID (kas lygu true) arba false priklausomai nuo to, ar sėkmingai įterpė
     */
    public function create($data){
        $database = new Database();

        $sql = "INSERT INTO `Reservation` (`Name`, `GuestsNumber`, `TimeReservation`) VALUES (?, ?, ?)";

        $data_to_insert = [
            $data['name'],
            $data['persons'],
            $data['time']
        ];

        $result = $database->executeSql($sql, $data_to_insert);

        return $result;
    }
}

//gauna duomenis is kotrolerio ir iraso i DB

