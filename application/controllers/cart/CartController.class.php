<?php

class CartController
{

    public function httpGetMethod(Http $http, array $queryFields) //Get ????
    {
        return [
            'cart' => $_SESSION["cart"],
            'grand_total' => array_field_sum($_SESSION['cart'], "total")
        ];
    }

    // cia pasiima duomenis  visus kuriuos turime orderyje ir kuriuos turesime irasyti i DB

    public function httpPostMethod(Http $http, array $formFields)
    {
        if (isset($formFields['type']) && $formFields['type'] == 'remove') {
            // jei paspaustas remove is cart mygtukas

            $id = $formFields['item_id'];

            foreach ($_SESSION['cart'] as $key => $cart_item) {
                if ($cart_item['id'] == $id) {
                    unset($_SESSION['cart'][$key]);
                    break;
                }
            }

            return [
                'cart' => $_SESSION["cart"],
                'grand_total' => array_field_sum($_SESSION['cart'], "total")
            ];
        } elseif (isset($formFields['type']) && $formFields['type'] == 'ajax') {
            // jei pridedam i cart per AJAX

            $orderProductID = $_POST["productID"];
            $orderProductQty = $_POST["productQty"];

            $ordersModel = new OrdersModel();

            $productInfo = $ordersModel->listProductId($orderProductID);

            $tax = 21;

            $cart_item = [
                "id" => $productInfo['id'],
                "photo" => "/images/meals/" . $productInfo['Photo'],
                "name" => $productInfo['Name'],
                "quantity" => $orderProductQty,
                "price" => $productInfo['SalePrice'],
                "tax" => ($orderProductQty * $productInfo['SalePrice']) * ($tax / 100), //dalis - kokia kainos dalis yra mokesciai
                "total" => ($orderProductQty * $productInfo['SalePrice']) * ($tax / 100 + 1)
            ];


            array_push($_SESSION['cart'], $cart_item);

            $ordersCount = array_field_sum($_SESSION['cart'], "quantity");

            //? acia todel kas siunciu per AJAX atsakyma JSON formatu

            $http->sendJsonResponse([
                'ordersCount' => $ordersCount,
                'orders' => $_SESSION['cart']
            ]);
        }

    }

}



// gauti is Orders modelio produkto ID





