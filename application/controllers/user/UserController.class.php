<?php

class UserController
{

    public function httpGetMethod(Http $http, array $formFields)
    {

        // Sukuriame modelio objekta

        $UserModel = new UserModel();



           // Kreipiames i modelio klases funkcija ir priskiriame reiksmei

            $user = $UserModel->listAll();

        // Perduodame reiksmes i vei
        return [
            'user' => $user,
        ];

    }


    public function httpPostMethod(Http $http, array $queryFields)
    {

        $UserModel = new UserModel();

        // geri formos duomenys
        $result = $UserModel->create($queryFields);

        if ($result) {
            // pavyko sukurti
            return $http->redirectTo('/user');   // nukreipiam i kita puslapi
        } else {
            // nepavyko sukurti
            die('nepavyko sukurti');
        }
    }

};


