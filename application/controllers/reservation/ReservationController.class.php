<?php

class ReservationController
{
    public function httpGetMethod(Http $http, array $queryFields)
    {

        // Sukuriame modelio objekta
        $ReservationModel = new ReservationModel();

        /*
         * Kreipiames i
        modelio klases funkcija ir
        priskiriame reiksmei
        */
        $reservation = $ReservationModel->listAll();


        // Perduodame reiksmes i vei
        return [
            'reservation' => $reservation,
        ];
    }

    /**
     * Naujos rezervacijos sukūrimas submitinus formą.
     *
     * @param Http $http
     * @param array $queryFields - formos duomenys
     */
    public function httpPostMethod(Http $http, array $queryFields)
    {
        //sukuriu nauja modelio objekta
        $ReservationModel = new ReservationModel();

        $result = $ReservationModel->create($queryFields);

    /*    dd($result);*/

        if($result){
            // pavyko sukurti
            return $http->redirectTo('/reservation');   // nukreipiam i kita puslapi
        }else{
            // nepavyko sukurti
        }
    }
}