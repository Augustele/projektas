<?php

class OrdersController
{

    public function httpPostMethod(Http $http, array $formFields)
    {

        $ordersModel = new OrdersModel();

        if(!isset($_SESSION['logged']) || (isset($_SESSION['logged']) && !$_SESSION['is_admin'])) {

            if (isset($formFields['type']) && $formFields['type'] == 'remove') {
                // jei paspaustas remove is cart mygtukas - dabar reik pakeisti jei pasapaustas lange Order mygtukas remove

                $id = $formFields['item_id'];

                foreach ($_SESSION['cart'] as $key => $cart_item) {
                    if ($cart_item['id'] == $id) {
                        unset($_SESSION['cart'][$key]);
                        break;
                    }
                }

                return [
                    'cart' => $_SESSION["cart"],
                    'grand_total' => array_field_sum($_SESSION['cart'], "total")
                ];
            }

        }else {


            if (isset($formFields['type']) && $formFields['type'] == 'remove') {
                // jei paspaustas remove is cart mygtukas - dabar reik pakeisti jei pasapaustas lange Order mygtukas remove

                $id = $formFields['item_id'];

                $ordersModel->delete($id);

                $http->redirectTo('/orders');
            }

            if (isset($formFields['type']) && $formFields['type'] == 'create_meal') {

                $ordersModel->createMeal($formFields);

                $http->redirectTo('/orders');
            }

        }

    }




    public function httpGetMethod(Http $http, array $queryFields)
    {


        // Sukuriame modelio objekta
        $ordersModel = new OrdersModel();

        /*
         * Kreipiames i
        modelio klases funkcija ir
        priskiriame reiksmei
        */
        $orders = $ordersModel->listAll();
        $QuantityTotal = $this->toDoCount($orders);

        $ordersCount = array_field_sum($_SESSION['cart'], "quantity");


        // Perduodame reiksmes i vei
        return [
            'ordersCount' => $ordersCount,
            'orders' => $orders,
            'quantity' => $QuantityTotal,
            'cart' => $_SESSION["cart"]
        ];
    }


    public function toDoCount($orders)
    {
        $total = 0;
        foreach ($orders as $order)
        {
            $total += $order['QuantityInStock'];
        }
        return $total;
    }

}

/*
ka i vaizda graziname nustatome per return. Return statmentas nustato kokiu pavidalu naudosim kitamuoseius savo viws faile*/



