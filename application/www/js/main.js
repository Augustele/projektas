'use strict';

/////////////////////////////////////////////////////////////////////////////////////////
// FONCTIONS                                                                           //
/////////////////////////////////////////////////////////////////////////////////////////
var baseURL = "";
/*var baseURL = "http://localhost/projektas/index.php";*/
$(document).ready(function () {
    $(".addToCart").click(function(e) {
        e.preventDefault();
        var productID =  $(this).data("productid");
        var productQty = $(this).parent().parent().find('input[type=number]').val();

        if(productQty > 0) {
            makeAddToCartCall(productID, productQty);
        }
    });

    function makeAddToCartCall(productID, productQty) {
        $.ajax({
            method: "POST",
            url: baseURL + "cart",
            data: {
                productID : productID,
                productQty : productQty,
                type: 'ajax'
            }

        }).done(function( msg, data ) {
            console.log(msg);
            var result = JSON.parse(msg);
            $("#ordersCount").text(result.ordersCount);
         });
    }
});








/////////////////////////////////////////////////////////////////////////////////////////
// CODE PRINCIPAL                                                                      //
/////////////////////////////////////////////////////////////////////////////////////////

