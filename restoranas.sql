-- phpMyAdmin SQL Dump
-- version 4.5.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 30, 2016 at 08:57 AM
-- Server version: 5.7.11
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `restoranas`
--

-- --------------------------------------------------------

--
-- Table structure for table `meal`
--

CREATE TABLE `meal` (
  `id` int(11) NOT NULL,
  `Name` varchar(30) NOT NULL,
  `Photo` varchar(30) NOT NULL,
  `Description` varchar(250) NOT NULL,
  `QuantityInStock` tinyint(4) NOT NULL,
  `BuyPrice` double NOT NULL,
  `SalePrice` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `meal`
--

INSERT INTO `meal` (`id`, `Name`, `Photo`, `Description`, `QuantityInStock`, `BuyPrice`, `SalePrice`) VALUES
(16, 'Deser', 'Desertas2.jpg', 'Le Banon est un fromage de chèvre au lait cru et entier, mûri dans des .... cylindrique verticale légèrement bombée, à pâte de couleur crème, ferme et lisse.', 3, 3, 6),
(17, 'Limone', 'Citrinos1.jpg', 'Le Banon est un fromage de chèvre au lait cru et entier, mûri dans des .... cylindrique verticale légèrement bombée, à pâte de couleur crème, ferme et lisse.', 3, 4, 7),
(18, 'Caramele', 'karamele1.jpg', 'Le Banon est un fromage de chèvre au lait cru et entier, mûri dans des .... cylindrique verticale légèrement bombée, à pâte de couleur crème, ferme et lisse.', 2, 6, 8),
(20, 'Alkoholinis', 'valgomasalkoholinis1.jpg', 'Le Banon est un fromage de chèvre au lait cru et entier, mûri dans des .... cylindrique verticale légèrement bombée, à pâte de couleur crème, ferme et lisse.', 3, 5, 8),
(21, 'cremeux', 'cremeux1.jpg', 'Le Banon est un fromage de chèvre au lait cru et entier, mûri dans des .... cylindrique verticale légèrement bombée, à pâte de couleur crème, ferme et lisse.', 4, 7, 10),
(22, 'Rosa', 'Roze1.jpg', 'Le Banon est un fromage de chèvre au lait cru et entier, mûri dans des .... cylindrique verticale légèrement bombée, à pâte de couleur crème, ferme et lisse.', 4, 10, 14);

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE `order` (
  `Id` int(11) NOT NULL,
  `User_Id` int(11) NOT NULL,
  `TotalAmount` double NOT NULL,
  `TaxRate` float NOT NULL,
  `TaxAmount` double NOT NULL,
  `CreationTimestamp` datetime NOT NULL,
  `CompleateTimestamp` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order`
--

INSERT INTO `order` (`Id`, `User_Id`, `TotalAmount`, `TaxRate`, `TaxAmount`, `CreationTimestamp`, `CompleateTimestamp`) VALUES
(1, 4, 0, 21, 4.83, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 5, 0, 21, 5.985, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 4, 0, 21, 7.875, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 4, 0, 21, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 4, 0, 21, 1.4175, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `orderline`
--

CREATE TABLE `orderline` (
  `Id` int(11) NOT NULL,
  `QuantityOrdered` int(4) NOT NULL,
  `Meal_Id` int(11) NOT NULL,
  `Order_Id` int(11) NOT NULL,
  `PriceEach` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orderline`
--

INSERT INTO `orderline` (`Id`, `QuantityOrdered`, `Meal_Id`, `Order_Id`, `PriceEach`) VALUES
(1, 2, 2, 1, 13.31),
(2, 4, 1, 1, 14.52),
(3, 3, 1, 2, 10.89),
(4, 1, 2, 2, 6.655),
(5, 1, 1, 2, 3.63),
(6, 2, 2, 2, 13.31),
(17, 3, 3, 4, 45.375),
(18, 1, 4, 6, 8.1675);

-- --------------------------------------------------------

--
-- Table structure for table `reservation`
--

CREATE TABLE `reservation` (
  `id` int(11) NOT NULL,
  `Name` text NOT NULL,
  `GuestsNumber` int(3) NOT NULL,
  `DateReservation` date NOT NULL,
  `TimeReservation` time NOT NULL,
  `DateCreate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `reservation`
--

INSERT INTO `reservation` (`id`, `Name`, `GuestsNumber`, `DateReservation`, `TimeReservation`, `DateCreate`) VALUES
(3, 'Petras', 5, '0000-00-00', '13:45:00', '2016-08-12 11:42:12'),
(4, 'Jonas', 9, '0000-00-00', '14:00:00', '2016-08-12 11:44:04'),
(5, 'Jonas', 9, '0000-00-00', '14:00:00', '2016-08-12 11:45:07'),
(6, 'delfi', 4, '0000-00-00', '20:10:00', '2016-08-12 11:50:27'),
(7, 'gy', 2, '0000-00-00', '20:10:00', '2016-08-18 16:30:47'),
(8, 's', 1, '0000-00-00', '00:20:15', '2016-08-18 16:31:25'),
(9, 'Jurga', 1, '0000-00-00', '06:15:00', '2016-08-18 16:42:36');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `Id` int(11) NOT NULL,
  `FirstName` varchar(40) NOT NULL,
  `LastName` varchar(20) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `Password` varchar(64) NOT NULL,
  `BirthDate` date NOT NULL,
  `Address` varchar(250) NOT NULL,
  `City` varchar(40) NOT NULL,
  `ZipCode` char(5) NOT NULL,
  `Country` varchar(20) NOT NULL,
  `Phone` char(10) NOT NULL,
  `CreationTimestamp` datetime NOT NULL,
  `LastLoginTimestamp` datetime NOT NULL,
  `Role` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`Id`, `FirstName`, `LastName`, `Email`, `Password`, `BirthDate`, `Address`, `City`, `ZipCode`, `Country`, `Phone`, `CreationTimestamp`, `LastLoginTimestamp`, `Role`) VALUES
(3, 'j', 'j', 'ji@fer.fr', '50dd69a798e1afa468790c6e287124f9', '0000-00-00', '', '', '', '', '45', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(4, 'Jurga', 'Spurga', 'juaugu@gmail.com', '0cc175b9c0f1b6a831c399e269772661', '0000-00-00', '', '', '', '', '15', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(5, 'Adminas', 'Adminas', 'admin@restaurant.3wa', '0cc175b9c0f1b6a831c399e269772661', '0000-00-00', '', '', '', '', '123456', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(6, 'jurga', 'cv', 'juaugu@gmail.com', '8f51ef3b9040a527832bebba66e286ac', '0000-00-00', '', '', '', '', 'vd', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `meal`
--
ALTER TABLE `meal`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `User_Id` (`User_Id`);

--
-- Indexes for table `orderline`
--
ALTER TABLE `orderline`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `Order_Id` (`Order_Id`),
  ADD KEY `Meal_Id` (`Meal_Id`);

--
-- Indexes for table `reservation`
--
ALTER TABLE `reservation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`Id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `meal`
--
ALTER TABLE `meal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `orderline`
--
ALTER TABLE `orderline`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `reservation`
--
ALTER TABLE `reservation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `order`
--
ALTER TABLE `order`
  ADD CONSTRAINT `Order_ibfk_1` FOREIGN KEY (`User_Id`) REFERENCES `user` (`Id`) ON DELETE CASCADE;

--
-- Constraints for table `orderline`
--
ALTER TABLE `orderline`
  ADD CONSTRAINT `Orderline_ibfk_1` FOREIGN KEY (`Order_Id`) REFERENCES `order` (`Id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
